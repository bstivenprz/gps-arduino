#include <PubSubClient.h>

const char mqttServer[] = "181.132.147.176";
const int mqttPort = 1883;
const char mqttUser[] = "qcode";
const char mqttPswd[] = "qcode";
const char topicInit[] = "esp32/init";
const char topicStatus[] = "esp32/ledStatus";
const char topicGps[] = "esp32/gps";

PubSubClient mqtt(client);

uint32_t lastReconnectAttempt = 0;

void mqttCallback(char *topic, byte *payload, unsigned int len) {
  SerialMon.print("Mensaje de [");
  SerialMon.print(topic);
  SerialMon.println("]: ");
  SerialMon.write(payload, len);
  SerialMon.println();

  if (String(topic) == topicStatus) {
    ledStatus = !ledStatus;
    digitalWrite(LED_GPIO, ledStatus);
    mqtt.publish(topicStatus, ledStatus ? "{ \"ledStatus\": true }" : "{ \"ledStatus\": false }");
  }
}

boolean mqttConnect() {
  SerialMon.print("Conectando al servidor MQTT ");
  SerialMon.println(mqttServer);

  mqtt.setServer(mqttServer, mqttPort);
  boolean status = mqtt.connect("ESP32", mqttUser, mqttPswd);

  if (!status) {
    SerialMon.println("Conexión con el servidor MQTT fallido");
    return false;
  }

  SerialMon.println("Conexión con el servidor MQTT exitoso");
  delay(3000);
//  mqtt.publish(topicInit, "init");
//  mqtt.subscribe(topicStatus);
  return mqtt.connected();
}

void mqttReconnect() {
  SerialMon.println("Reintentando conectar al servidor MQTT");
  uint32_t t = millis();
  if (t - lastReconnectAttempt > 10000L) {
    lastReconnectAttempt = t;
    if (mqttConnect()) {
      lastReconnectAttempt = 0;
    }
  }
  delay(100);
  return;
}
