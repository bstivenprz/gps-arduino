#define SIM800L_IP5306_VERSION_20190610
// #define SIM800L_AXP192_VERSION_20200327
// #define SIM800C_AXP192_VERSION_20200609
// #define SIM800L_IP5306_VERSION_20200811

#include "utilities.h"

// Select your modem:
#define TINY_GSM_MODEM_SIM800

// Set serial for debug console (to the Serial Monitor, default speed 115200)
#define SerialMon Serial

// Set serial for AT commands (to the module)
// Use Hardware Serial on Mega, Leonardo, Micro
#define SerialAT Serial1

// See all AT commands, if wanted
#define DUMP_AT_COMMANDS

// Define the serial console for debug prints, if needed
#define TINY_GSM_DEBUG SerialMon

// Add a reception delay - may be needed for a fast processor at a slow baud rate
// #define TINY_GSM_YIELD() { delay(2); }

// Define how you're planning to connect to the internet
#define TINY_GSM_USE_GPRS true
#define TINY_GSM_USE_WIFI false

// set GSM PIN, if any
#define GSM_PIN ""

// Your GPRS credentials, if any
const char apn[] = "internet.movistar.com.co";
const char gprsUser[] = "movistar";
const char gprsPass[] = "movistar";

#include <TinyGsmClient.h>

#ifdef DUMP_AT_COMMANDS
#include <StreamDebugger.h>
StreamDebugger debugger(SerialAT, SerialMon);
TinyGsm modem(debugger);
#else
TinyGsm modem(SerialAT);
#endif

TinyGsmClient client(modem);

int ledStatus = LOW;

// Variables for storing GPS Data
float latitude;
float longitude;
float speed;
float satellites;
String direction;

#include "mqtt.h"

#include <SoftwareSerial.h>

#include "TinyGPS++.h"

TinyGPSPlus gps;
TinyGPSCustom stats(gps, "GPRMC", 10);
SoftwareSerial SerialGPS(14, 27);

void setup() {
    // Set console baud rate
    SerialMon.begin(115200);

    delay(10);

    setupModem();

    SerialMon.println("Wait...");

    // Set GSM module baud rate and UART pins
    SerialAT.begin(115200, SERIAL_8N1, MODEM_RX, MODEM_TX);

    delay(6000);

    // Restart takes quite some time
    // To skip it, call init() instead of restart()
    SerialMon.println("Initializing modem...");
    modem.restart();
    // modem.init();

    String modemInfo = modem.getModemInfo();
    SerialMon.print("Modem Info: ");
    SerialMon.println(modemInfo);

    #if TINY_GSM_USE_GPRS
        // Unlock your SIM card with a PIN if needed
        if ( GSM_PIN && modem.getSimStatus() != 3 ) {
            modem.simUnlock(GSM_PIN);
        }
    #endif

    SerialMon.print("Waiting for network...");
    if (!modem.waitForNetwork()) {
        SerialMon.println(" fail");
        delay(10000);
        return;
    }
    SerialMon.println(" success");

    if (modem.isNetworkConnected()) {
        SerialMon.println("Network connected");
    }

    // GPRS connection parameters are usually set after network registration
    SerialMon.print(F("Connecting to "));
    SerialMon.print(apn);
    if (!modem.gprsConnect(apn, gprsUser, gprsPass)) {
        SerialMon.println(" fail");
        delay(10000);
        return;
    }
    SerialMon.println(" success");

    if (modem.isGprsConnected()) {
        SerialMon.println("GPRS connected");
    }
//
//    mqttConnect();
//    mqtt.setCallback(mqttCallback);
}

void displayInfo() {
 if (gps.location.isValid() )
  {

    latitude = gps.location.lat();     //Storing the Lat. and Lon.
    longitude = gps.location.lng();

    Serial.print("LAT:  ");
    Serial.println(latitude, 6);  // float to x decimal places
    Serial.print("LONG: ");
    Serial.println(longitude, 6);
    speed = gps.speed.kmph();               //get speed
    direction = TinyGPSPlus::cardinal(gps.course.value()); // get the directions like east west north south
    satellites = gps.satellites.value();    //get number of satellites
  }
  //Serial.println();
}

void loop() {
  if (!mqtt.connected()) {
    mqttReconnect();
  }

  while (Serial.available()) {
    gps.encode(Serial.read());
    Serial.println(stats.value());
    mqtt.publish(topicGps, stats.value());
  }
  mqtt.loop();
}
